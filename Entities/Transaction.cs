using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using TakeIt.WebApi.Enums;

namespace TakeIt.WebApi.Entities
{
    [BsonIgnoreExtraElements]
    public class Transaction
    {
        public Transaction() {}

        // Default create at TransactionState.Pending
        public Transaction(string giver, string receiver, string post)
        {
            Giver = new ObjectId(giver);
            Receiver = new ObjectId(receiver);
            Post = new ObjectId(post);
            CreatedAt = DateTime.Now;
            State = TransactionState.Pending;
        }

        // For update
        public Transaction(string id, string giver, string receiver, string post,
            DateTime createdAt, TransactionState state)
        {
            Id = id;
            Giver = new ObjectId(giver);
            Receiver = new ObjectId(receiver);
            Post = new ObjectId(post);
            CreatedAt = createdAt;
            State = state;
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("giver")]
        public ObjectId Giver { get; set; }

        [BsonElement("receiver")]
        public ObjectId Receiver { get; set; }

        [BsonElement("post")]
        public ObjectId Post { get; set; }

        [BsonElement("created_at")]
        public DateTime CreatedAt { get; set; }

        [BsonElement("state")]
        public TransactionState State { get; set; }
    }
}