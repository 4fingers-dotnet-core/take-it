using System.Collections.Generic;
using System.Text.Json.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using TakeIt.WebApi.Enums;

namespace TakeIt.WebApi.Entities
{
    [BsonIgnoreExtraElements]
    public class Province
    {
        public Province() {}

        public Province(string name)
        {
            Name = name;
        }

        public Province(string id, string name)
        {
            Id = id;
            Name = name;
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }
    }
}