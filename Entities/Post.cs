using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using TakeIt.WebApi.Enums;
using TakeIt.WebApi.Helpers;

namespace TakeIt.WebApi.Entities
{
    [BsonIgnoreExtraElements]
    public class Post
    {
        public Post() {}

        // Default for new insert
        public Post(string owner, string category, string province, int type, string title,
            string content, string location, List<string> items)
        {
            Owner = new ObjectId(owner);
            Category = new ObjectId(category);
            Province = new ObjectId(province);
            Type = (PostType) type;
            Title = title;
            Content = content;
            Location = location;
            Items = ObjectIdHelper.FromList(items);
            CreatedAt = DateTime.Now;
            State = PostState.Pending;
        }

        // For admin update
        public Post(string id, string owner, string category, string province, int type,
            string title, string content, string location, List<string> items, DateTime createdAt,
            PostState state)
        {
            Id = id;
            Owner = new ObjectId(owner);
            Category = new ObjectId(category);
            Province = new ObjectId(province);
            Type = (PostType) type;
            Title = title;
            Content = content;
            Location = location;
            Items = ObjectIdHelper.FromList(items);
            CreatedAt = createdAt;
            State = state;
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("owner")]
        public ObjectId Owner { get; set; }

        [BsonElement("category")]
        public ObjectId Category { get; set; }

        [BsonElement("province")]
        public ObjectId Province { get; set; }

        [BsonElement("type")]
        public PostType Type { get; set; }

        [BsonElement("title")]
        public string Title { get; set; }

        [BsonElement("content")]
        public string Content { get; set; }

        [BsonElement("location")]
        public string Location { get; set; }

        [BsonElement("items")]
        public List<ObjectId> Items { get; set; }

        [BsonElement("created_at")]
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime CreatedAt { get; set; }

        [BsonElement("state")]
        public PostState State { get; set; }
    }
}