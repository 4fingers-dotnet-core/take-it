using System.Text.Json.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using TakeIt.WebApi.Enums;

namespace TakeIt.WebApi.Entities
{
    [BsonIgnoreExtraElements]
    public class User
    {
        public User() {}
        public User(string username, string password)
        {
            FirstName = "";
            LastName = "";
            Username = username;
            Hash = BCrypt.Net.BCrypt.HashPassword(password);
            Token = "";
            State = UserState.Active;
            Level = UserLevel.Member;
        }

        public User(string id, string firstName, string lastName, string username, string hash,
            string token, UserLevel level, UserState state)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Username = username;
            Hash = hash;
            Token = token;
            State = state;
            Level = level;
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("firstName")]
        public string FirstName { get; set; }

        [BsonElement("lastName")]
        public string LastName { get; set; }

        [BsonElement("username")]
        public string Username { get; set; }

        [BsonElement("hash")]
        [JsonIgnore]
        public string Hash { get; set; }

        [BsonElement("token")]
        public string Token { get; set; }

        [BsonElement("level")]
        public UserLevel Level { get; set; }

        [BsonElement("state")]
        public UserState State { get; set; }
    }
}