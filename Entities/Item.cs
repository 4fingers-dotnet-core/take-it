using System.Collections.Generic;
using System.Text.Json.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using TakeIt.WebApi.Enums;

namespace TakeIt.WebApi.Entities
{
    [BsonIgnoreExtraElements]
    public class Item
    {
        public Item() {}

        public Item(string owner, string name, float quantity, string unit, List<string> images,
            ItemState state)
        {
            Owner = new ObjectId(owner);
            Name = name;
            Quantity = quantity;
            Unit = unit;
            Images = images;
            State = state;
        }

        public Item(string id, string owner, string name, float quantity, string unit, List<string> images,
            ItemState state)
        {
            Id = id;
            Owner = new ObjectId(owner);
            Name = name;
            Quantity = quantity;
            Unit = unit;
            Images = images;
            State = state;
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("owner")]
        public ObjectId Owner { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("quantity")]
        public float Quantity { get; set; }

        [BsonElement("unit")]
        public string Unit { get; set; }

        [BsonElement("images")]
        public List<string> Images { get; set; }

        [BsonElement("state")]
        public ItemState State { get; set; }
    }
}