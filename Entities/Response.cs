using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using TakeIt.WebApi.Enums;

namespace TakeIt.WebApi.Entities
{
    [BsonIgnoreExtraElements]
    public class Response
    {
        public Response() {}

        public Response(string reporter, string subject, string content, ResponseType type)
        {
            Reporter = new ObjectId(reporter);
            Subject = new ObjectId(subject);
            Content = content;
            AdminMessage = "";
            CreatedAt = DateTime.Now;
            Type = type;
            State = ResponseState.Pending;
        }

        public Response(string id, string reporter, string subject, string content, string adminMessage,
            DateTime createdAt, ResponseType type, ResponseState state)
        {
            Id = id;
            Subject = new ObjectId(subject);
            Reporter = new ObjectId(reporter);
            Content = content;
            AdminMessage = adminMessage;
            CreatedAt = createdAt;
            Type = type;
            State = state;
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("reporter")]
        public ObjectId Reporter { get; set; }

        [BsonElement("subject")]
        public ObjectId Subject { get; set; }

        [BsonElement("content")]
        public string Content { get; set; }

        [BsonElement("admin_message")]
        public string AdminMessage { get; set; }

        [BsonElement("created_at")]
        public DateTime CreatedAt { get; set; }

        [BsonElement("type")]
        public ResponseType Type { get; set; }

        [BsonElement("state")]
        public ResponseState State { get; set; }
    }
}