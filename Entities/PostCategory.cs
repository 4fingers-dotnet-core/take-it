using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using TakeIt.WebApi.Enums;

namespace TakeIt.WebApi.Entities
{
    [BsonIgnoreExtraElements]
    public class PostCategory
    {
        public PostCategory() {}

        public PostCategory(string name, string description)
        {
            Name = name;
            Description = description;
            State = PostCategoryState.Available;
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("description")]
        public string Description { get; set; }

        [BsonElement("state")]
        public PostCategoryState State { get; set; }
    }
}