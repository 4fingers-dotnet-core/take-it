using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TakeIt.WebApi.Entities;
using TakeIt.WebApi.Enums;
using TakeIt.WebApi.IServices;
using TakeIt.WebApi.Models.Post;
using TakeIt.WebApi.Services;

namespace TakeIt.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly IPostService _postService;

        public PostsController(IPostService postService)
        {
            _postService = postService;
        }

        [AllowAnonymous]
        [HttpGet("filter")]
        public IActionResult Filter(string userId, int? type, string category, string province,
            int? state, ReturnType reTurnType) {
            var posts = _postService.Filter(userId, type, category, province, state, reTurnType);
            
            if (posts.Count > 0)
                return Ok(posts);
            else
                return BadRequest();
        }

        [AllowAnonymous]
        [HttpGet("{id:length(24)}", Name = "GetPost")]
        public ActionResult<dynamic> Get(string id, ReturnType returnType)
        {
            var post = _postService.Get(id, returnType);

            if (post == null)
            {
                return NotFound();
            }

            return post;
        }

        [HttpPost]
        public ActionResult<Post> Create(PostModel postModel)
        {
        	if (!ModelState.IsValid)
			{
			    return BadRequest(ModelState);
			}
			else
			{
                Post post = new Post(
                    postModel.Owner, postModel.Category, postModel.Province, postModel.Type, postModel.Title,
                    postModel.Content, postModel.Location, postModel.Items
                );
                
				_postService.Create(post);

            	return CreatedAtRoute("GetPost", new { id = post.Id.ToString(), returnType = ReturnType.Basic }, post);
			}
        }

        [HttpPut("{id:length(24)}")]
        public IActionResult Update(string id, PostModel postModel)
        {
            var post = _postService.Get(id, ReturnType.Basic);

            if (post == null)
            {
                return NotFound();
            }

            _postService.Update(
                id,
                new Post(
                    id, postModel.Owner, postModel.Category, postModel.Province, postModel.Type, postModel.Title,
                    postModel.Content, postModel.Location, postModel.Items, postModel.CreatedAt,
                    postModel.State
                )
            );

            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var post = _postService.Get(id, ReturnType.Basic);

            if (post == null)
            {
                return NotFound();
            }

            _postService.Remove(post.Id);

            return NoContent();
        }
    }
}