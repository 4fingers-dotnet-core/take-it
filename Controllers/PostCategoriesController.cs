using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TakeIt.WebApi.Entities;
using TakeIt.WebApi.IServices;
using TakeIt.WebApi.Models.PostCategory;
using TakeIt.WebApi.Services;

namespace TakeIt.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PostCategoriesController : ControllerBase
    {
        private readonly IPostCategoryService _postCategoryService;

        public PostCategoriesController(IPostCategoryService postCategoryService)
        {
            _postCategoryService = postCategoryService;
        }

        [AllowAnonymous] 
        [HttpGet("filter")]
        public IActionResult Filter(string name) {
            var postCategories = _postCategoryService.Filter(name);
            
            if (postCategories.Count > 0)
                return Ok(postCategories);
            else
                return BadRequest();
        }

        [HttpGet("{id:length(24)}", Name = "GetPostCategory")]
        public ActionResult<PostCategory> Get(string id)
        {
            var postCategory = _postCategoryService.Get(id);

            if (postCategory == null)
            {
                return NotFound();
            }

            return postCategory;
        }

        [HttpPost]
        public ActionResult<PostCategory> Create([FromBody]PostCategoryModel postCategoryModel)
        {
        	if (!ModelState.IsValid)
			{
			    return BadRequest(ModelState);
			}
			else
			{
                var postCategory = new PostCategory(postCategoryModel.Name,
                    postCategoryModel.Description);
				_postCategoryService.Create(postCategory);

            	return CreatedAtRoute("GetPostCategory", new { id = postCategory.Id.ToString() },
                    postCategory);
			}
        }

        [HttpPut("{id:length(24)}")]
        public IActionResult Update(string id, PostCategory postCategoryIn)
        {
            var postCategory = _postCategoryService.Get(id);

            if (postCategory == null)
            {
                return NotFound();
            }

            _postCategoryService.Update(id, postCategoryIn);

            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var postCategory = _postCategoryService.Get(id);

            if (postCategory == null)
                return NotFound();

            _postCategoryService.Remove(postCategory.Id);

            return NoContent();
        }
    }
}