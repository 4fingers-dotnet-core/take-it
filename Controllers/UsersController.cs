﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using TakeIt.WebApi.Services;
using TakeIt.WebApi.Models;
using System.Threading.Tasks;
using TakeIt.WebApi.Entities;
using System;
using TakeIt.WebApi.IServices;
using System.Collections.Generic;
using TakeIt.WebApi.Enums;
using TakeIt.WebApi.Models.User;

namespace TakeIt.WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private IUserService _userService;
        private IItemService _itemService;

        public UsersController(IUserService userService, IItemService itemService)
        {
            _userService = userService;
            _itemService = itemService;
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult Register([FromBody]RegisterModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            else {
                User user = new User(model.Username, model.Password);
                var createdUser = _userService.Create(user);

                if (createdUser == null)
                    return BadRequest(new { message = "Username or password is incorrect" });

                return CreatedAtRoute("GetUser", new { id = user.Id.ToString() }, user);
            }
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody]AuthenticateModel model)
        {
            var user = _userService.Authenticate(model.Username, model.Password);

            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return new OkObjectResult(user);
        }

        [HttpGet("filter")]
        public async Task<IActionResult> Filter(int? state, ReturnType returnType)
        {
            dynamic users;
            users = await _userService.Filter(state, returnType);

            if (users.Count > 0)
                return Ok(users);
            else
                return BadRequest();
        }

        [HttpGet("{id:length(24)}", Name = "GetUser")]
        public async Task<ActionResult<User>> Get(string id)
        {
            var user = await _userService.Get(id);

            if (user == null)
            {
                return NotFound();
            }

            return user;
        }

        [HttpGet("{id:length(24)}/items")]
        public ActionResult<List<Item>> GetByUserId(string id)
        {
            var items = _itemService.GetByUserId(id);

            if (items == null)
            {
                return NotFound();
            }

            return Ok(items);
        }
        
        [HttpPut("{id:length(24)}")]
        public IActionResult Update(string id, UserModel userModel)
        {
            var user = _userService.Get(id);

            if (user == null)
            {
                return NotFound();
            }

            _userService.Update(
                id,
                new User(
                    id, userModel.FirstName, userModel.LastName, userModel.Username,
                    userModel.Hash, userModel.Token, userModel.Level, userModel.State
                )
            );

            return NoContent();
        }
    }
}
