using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TakeIt.WebApi.Entities;
using TakeIt.WebApi.Enums;
using TakeIt.WebApi.IServices;
using TakeIt.WebApi.Models.Item;
using TakeIt.WebApi.Services;

namespace TakeIt.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ItemsController : ControllerBase
    {
        private readonly IItemService _itemService;

        public ItemsController(IItemService itemService)
        {
            _itemService = itemService;
        }
        
        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetAll() {
            var items = _itemService.GetAll();
            
            if (items.Count > 0)
                return Ok(items);
            else
                return BadRequest();
        }

        [HttpGet("filter")]
        public IActionResult Filter(string owner, int? state, ReturnType returnType) {
            var items = _itemService.Filter(owner, state, returnType);
            
            if (items.Count > 0)
                return Ok(items);
            else
                return BadRequest();
        }

        [HttpGet("{id:length(24)}", Name = "GetItem")]
        public ActionResult<DetailItemModel> Get(string id, ReturnType returnType)
        {
            var item = _itemService.Get(id, returnType);

            if (item == null)
            {
                return NotFound();
            }

            return Ok(item);
        }

        [HttpPost]
        public ActionResult<Item> Create([FromBody]ItemModel itemModel)
        {
        	if (!ModelState.IsValid)
			{
			    return BadRequest(ModelState);
			}
			else
			{
                // Create new item ưith pending state (0)
                Item item = new Item(itemModel.Owner, itemModel.Name, itemModel.Quantity,
                    itemModel.Unit, itemModel.Images, ItemState.Pending);
                _itemService.Create(item);

            	return CreatedAtRoute("GetItem", new { id = item.Id.ToString() }, item);
			}
        }

        [HttpPut("{id:length(24)}")]
        public IActionResult Update(string id, [FromBody]ItemModel itemModel)
        {
            var item = _itemService.Get(id, ReturnType.Basic);

            if (item == null)
            {
                return NotFound();
            }

            // Only for admin
            _itemService.Update(id, new Item(itemModel.Id, itemModel.Owner, itemModel.Name, itemModel.Quantity, itemModel.Unit, itemModel.Images, itemModel.State));

            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var item = _itemService.Get(id, ReturnType.Basic);

            if (item == null)
            {
                return NotFound();
            }

            _itemService.Remove(item.Id);

            return NoContent();
        }
    }
}