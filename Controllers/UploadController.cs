using System;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Configuration;
using Minio;
using Minio.Exceptions;

namespace TakeIt.WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UploadController : ControllerBase
    {
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly IConfiguration Configuration;

        public UploadController(IWebHostEnvironment hostingEnvironment, IConfiguration configuration)
        {
            _hostingEnvironment = hostingEnvironment;
            Configuration = configuration;
        }

        [HttpPost]
        public async Task<IActionResult> Upload()
        {
            string[] _permittedImageExtensions = { ".jpg", ".jpeg", ".png" };
            var files = Request.Form.Files;
            var type = Request.Form["type"];

            if (files.Count == 0)
                return new BadRequestObjectResult(files);
            else
            {
                var file = files[0];
                string fileName = ContentDispositionHeaderValue
                                    .Parse(file.ContentDisposition)
                                    .FileName
                                    .Trim('"');
                string fileBaseName = Path.GetFileNameWithoutExtension(fileName);
                string fileExtension = Path.GetExtension(fileName).ToLowerInvariant();

                if (string.IsNullOrEmpty(fileExtension) || !_permittedImageExtensions.Contains(fileExtension)) {
                    return new BadRequestObjectResult("Định dạng file không phù hợp!");
                }

                string folder = Path.Combine(_hostingEnvironment.WebRootPath, "img", "uploaded", type);

                if (!Directory.Exists(folder))
                    Directory.CreateDirectory(folder);

                string newFileName = $"{DateTime.Now.Ticks}{fileExtension}";

                string filePath = Path.Combine(folder, newFileName);

                using (FileStream fs = System.IO.File.Create(filePath))
                {
                    file.CopyTo(fs);
                    fs.Flush();
                }

                string endpoint  = Configuration["MinIOSettings:Endpoint"];
                string accessKey = Configuration["MinIOSettings:AccessKey"];
                string secretKey = Configuration["MinIOSettings:SecretKey"];
                var minio = new MinioClient(endpoint, accessKey, secretKey);//.WithSSL();

                string bucketName = Configuration["MinIOSettings:BucketName"];
                string location   = Configuration["MinIOSettings:Location"];
                string objectName = $"{type}/{newFileName}";
                string contentType = "";
                new FileExtensionContentTypeProvider().TryGetContentType(fileName, out contentType);

                try
                {
                    // Make a bucket on the server, if not already present.
                    bool found = await minio.BucketExistsAsync(bucketName);
                    if (!found)
                    {
                        await minio.MakeBucketAsync(bucketName, location);
                    }
                    // Upload a file to bucket.
                    await minio.PutObjectAsync(bucketName, objectName, filePath, contentType);
                    Console.WriteLine("Successfully uploaded " + objectName );

                    return Ok($"http://{endpoint}/{bucketName}/{objectName}");
                }
                catch (MinioException e)
                {
                    Console.WriteLine("File Upload Error: {0}", e.Message);
                    return BadRequest(e.Message);
                }
                finally
                {
                    // Clean the unused file
                    System.IO.File.Delete(filePath);
                }
            }
        }
    }
}