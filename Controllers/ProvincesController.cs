using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TakeIt.WebApi.Entities;
using TakeIt.WebApi.Enums;
using TakeIt.WebApi.IServices;
using TakeIt.WebApi.Models.Province;
using TakeIt.WebApi.Services;

namespace TakeIt.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProvincesController : ControllerBase
    {
        private readonly IProvinceService _provinceService;

        public ProvincesController(IProvinceService provinceService)
        {
            _provinceService = provinceService;
        }

        [AllowAnonymous]
        [HttpGet("filter")]
        public IActionResult Filter(string name) {
            var provinces = _provinceService.Filter(name);
            
            if (provinces.Count > 0)
                return Ok(provinces);
            else
                return BadRequest();
        }

        [AllowAnonymous]
        [HttpGet("{id:length(24)}", Name = "GetProvince")]
        public ActionResult<Province> Get(string id)
        {
            var province = _provinceService.Get(id);

            if (province == null)
            {
                return NotFound();
            }

            return province;
        }

        [HttpPost]
        public ActionResult<Province> Create(ProvinceModel provinceModel)
        {
        	if (!ModelState.IsValid)
			{
			    return BadRequest(ModelState);
			}
			else
			{
                Province province = new Province(
                    provinceModel.Name
                );
                
				_provinceService.Create(province);

            	return CreatedAtRoute("GetProvince", new { id = province.Id.ToString() }, province);
			}
        }

        [HttpPut("{id:length(24)}")]
        public IActionResult Update(string id, ProvinceModel provinceModel)
        {
            var province = _provinceService.Get(id);

            if (province == null)
            {
                return NotFound();
            }

            _provinceService.Update(
                id,
                new Province(
                    id, provinceModel.Name
                )
            );

            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var province = _provinceService.Get(id);

            if (province == null)
            {
                return NotFound();
            }

            _provinceService.Remove(province.Id);

            return NoContent();
        }
    }
}