using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TakeIt.WebApi.Entities;
using TakeIt.WebApi.Enums;
using TakeIt.WebApi.IServices;
using TakeIt.WebApi.Models.Response;
using TakeIt.WebApi.Models.Transaction;

namespace TakeIt.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ResponsesController : ControllerBase
    {
        private readonly IResponseService _responseService;

        public ResponsesController(IResponseService responseService)
        {
            _responseService = responseService;
        }

        [AllowAnonymous]
        [HttpGet("filter")]
        public IActionResult Filter(int? type, ReturnType reTurnType) {
            var responses = _responseService.Filter(type, reTurnType);
            
            if (responses.Count > 0)
                return Ok(responses);
            else
                return BadRequest();
        }

        [AllowAnonymous]
        [HttpGet("{id:length(24)}", Name = "GetResponse")]
        public ActionResult<dynamic> Get(string id, ReturnType returnType)
        {
            var response = _responseService.Get(id, returnType);

            if (response == null)
            {
                return NotFound();
            }

            return response;
        }

        // Use for create transaction only
        [HttpPost]
        public ActionResult<Response> Create(ResponseModel responseModel)
        {
        	if (!ModelState.IsValid)
			{
			    return BadRequest(ModelState);
			}
			else
			{
                var response = new Response(
                    responseModel.Reporter,
                    responseModel.Subject,
                    responseModel.Content,
                    responseModel.Type
                );
                
				_responseService.Create(response);

            	return CreatedAtRoute("GetResponse", new { id = response.Id.ToString(), returnType = ReturnType.Basic }, response);
			}
        }

        [HttpPut("{id:length(24)}")]
        public IActionResult Update(string id, ResponseModel responseModel)
        {
            var response = _responseService.Get(id, ReturnType.Basic);

            if (response == null)
            {
                return NotFound();
            }

            // Default
            _responseService.Update(
                id,
                new Response(
                    id,
                    responseModel.Reporter,
                    responseModel.Subject,
                    responseModel.Content,
                    responseModel.AdminMessage,
                    responseModel.CreatedAt,
                    responseModel.Type,
                    responseModel.State
                )
            );

            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var response = _responseService.Get(id, ReturnType.Basic);

            if (response == null)
            {
                return NotFound();
            }

            _responseService.Remove(response.Id);

            return NoContent();
        }
    }
}