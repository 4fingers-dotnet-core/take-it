using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TakeIt.WebApi.Entities;
using TakeIt.WebApi.Enums;
using TakeIt.WebApi.IServices;
using TakeIt.WebApi.Models.Item;
using TakeIt.WebApi.Services;

namespace TakeIt.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AnalysticsController : ControllerBase
    {
        private readonly IAnalysticService _analysticService;

        public AnalysticsController(IAnalysticService analysticService)
        {
            _analysticService = analysticService;
        }

        [HttpGet("overall")]
        public IActionResult OverAll() {
            var overAllData = _analysticService.GetOverAllData();
            
            return Ok(overAllData);
        }

        [HttpGet("chart")]
        public IActionResult Chart(AnalysticDataType type) {
            var analystics = _analysticService.GetChartData(type);
            
            return Ok(analystics);
        }
    }
}