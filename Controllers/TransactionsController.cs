using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TakeIt.WebApi.Entities;
using TakeIt.WebApi.Enums;
using TakeIt.WebApi.IServices;
using TakeIt.WebApi.Models.Transaction;

namespace TakeIt.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly ITransactionService _transactionService;

        public TransactionsController(ITransactionService transactionService)
        {
            _transactionService = transactionService;
        }

        [AllowAnonymous]
        [HttpGet("filter")]
        public IActionResult Filter(string userId, int? state, ReturnType reTurnType) {
            var transactions = _transactionService.Filter(userId, state, reTurnType);
            
            if (transactions.Count > 0)
                return Ok(transactions);
            else
                return BadRequest();
        }

        [AllowAnonymous]
        [HttpGet("{id:length(24)}", Name = "GetTransaction")]
        public ActionResult<dynamic> Get(string id, ReturnType returnType)
        {
            var transaction = _transactionService.Get(id, returnType);

            if (transaction == null)
            {
                return NotFound();
            }

            return transaction;
        }

        // Use for create transaction only
        [HttpPost]
        public ActionResult<Transaction> Create(TransactionModel transactionModel)
        {
        	if (!ModelState.IsValid)
			{
			    return BadRequest(ModelState);
			}
			else
			{
                var transaction = new Transaction(
                    transactionModel.Giver,
                    transactionModel.Receiver,
                    transactionModel.Post
                );
                
				_transactionService.Create(transaction);

            	return CreatedAtRoute("GetTransaction", new { id = transaction.Id.ToString(), returnType = ReturnType.Basic }, transaction);
			}
        }

        [HttpPut("{id:length(24)}")]
        public IActionResult Update(string id, TransactionModel transactionModel)
        {
            var transaction = _transactionService.Get(id, ReturnType.Basic);

            if (transaction == null)
            {
                return NotFound();
            }

            // Approve
            if (transaction.State != TransactionState.Pending
                && transactionModel.State == TransactionState.InTransaction)
                return BadRequest();

            if (transaction.State == TransactionState.Pending
                && transactionModel.State == TransactionState.InTransaction)
            {
                _transactionService.Approve(
                    id,
                    new Transaction(
                        id,
                        transactionModel.Giver,
                        transactionModel.Receiver,
                        transactionModel.Post,
                        transactionModel.CreatedAt,
                        transactionModel.State
                    )
                );

                return NoContent();
            }

            // Finish
            if (transaction.State != TransactionState.PendingComplete
                && transactionModel.State == TransactionState.Complete)
                return BadRequest();

            if (transaction.State == TransactionState.PendingComplete
                && transactionModel.State == TransactionState.Complete)
            {
                _transactionService.Finish(
                    id,
                    new Transaction(
                        id,
                        transactionModel.Giver,
                        transactionModel.Receiver,
                        transactionModel.Post,
                        transactionModel.CreatedAt,
                        transactionModel.State
                    )
                );

                return NoContent();
            }

            // Default
            _transactionService.Update(
                id,
                new Transaction(
                    id,
                    transactionModel.Giver,
                    transactionModel.Receiver,
                    transactionModel.Post,
                    transactionModel.CreatedAt,
                    transactionModel.State
                )
            );

            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var transaction = _transactionService.Get(id, ReturnType.Basic);

            if (transaction == null)
            {
                return NotFound();
            }

            _transactionService.Remove(transaction.Id);

            return NoContent();
        }
    }
}