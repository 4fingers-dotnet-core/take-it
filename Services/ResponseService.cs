using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using TakeIt.WebApi.Entities;
using TakeIt.WebApi.Enums;
using TakeIt.WebApi.Helpers;
using TakeIt.WebApi.IServices;
using TakeIt.WebApi.Models;
using TakeIt.WebApi.Models.Post;

namespace TakeIt.WebApi.Services
{
    public class ResponseService : IResponseService
    {
        private readonly IMongoCollection<Item> _items;
        private readonly IMongoCollection<Post> _posts;
        private readonly IMongoCollection<PostCategory> _postCategories;
        private readonly IMongoCollection<User> _users;
        private readonly IMongoCollection<Province> _provinces;
        private readonly IMongoCollection<Response> _responses;

        public ResponseService(ITakeItDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _items = database.GetCollection<Item>(settings.ItemsCollectionName);
            _posts = database.GetCollection<Post>(settings.PostsCollectionName);
            _postCategories = database.GetCollection<PostCategory>(settings.PostCategoriesCollectionName);
            _users = database.GetCollection<User>(settings.UsersCollectionName);
            _provinces = database.GetCollection<Province>(settings.ProvincesCollectionName);
            _responses = database.GetCollection<Response>(settings.ResponsesCollectionName);
        }

        public dynamic Filter(int? responseType, ReturnType returnType) {
            var query = _responses.AsQueryable();

            if (responseType.HasValue) {
                query = query.Where(r => r.Type == (ResponseType) responseType);
            }

            switch (returnType)
            {
                case ReturnType.Basic:
                    return query.ToList();

                case ReturnType.Full:
                    return ReturnModelHelper.ToDetailResponseModel(query.ToList(), _users,
                        _postCategories, _posts, _items, _provinces);

                default:
                    throw new System.Exception();
            }
        }

        public dynamic Get(string id, ReturnType returnType)
        {
            var query = _responses.Find<Response>(r => r.Id.Equals(id)).FirstOrDefault();

            switch (returnType)
            {
                case ReturnType.Basic:
                    return query;

                case ReturnType.Full:
                    return ReturnModelHelper.ToDetailResponseModel(query, _users,
                        _postCategories, _posts, _items, _provinces);

                default:
                    throw new System.Exception();
            }
        }

        public Response Create(Response response)
        {
            _responses.InsertOne(response);

            return response;
        }

        // Update transaction state after approval from giver / receiver or done
        public void Update(string id, Response response)
        {
            _responses.ReplaceOne(r => r.Id == id, response);
        }

        public void Remove(Response response)
        {
            var post = new Post();
            var user = new User();

            if (response.Type == ResponseType.Post)
            {
                post = _posts.Find<Post>(p => p.Id.Equals(response.Subject)).FirstOrDefault();
                post.State = PostState.Available;
                _posts.ReplaceOne(p => p.Id == post.Id, post);
            }

            if (response.Type == ResponseType.User){
                user = _users.Find<User>(u => u.Id.Equals(response.Subject)).FirstOrDefault();
                user.State = UserState.Active;
                _users.ReplaceOne(u => u.Id == user.Id, user);
            }

            _responses.DeleteOne(t => t.Id == response.Id);
        }
            
        public void Remove(string id)
        {
            var response = _responses.Find<Response>(r => r.Id == id).FirstOrDefault();

            var post = new Post();
            var user = new User();

            if (response.Type == ResponseType.Post)
            {
                post = _posts.Find<Post>(p => p.Id.Equals(response.Subject)).FirstOrDefault();
                post.State = PostState.Available;
                _posts.ReplaceOne(p => p.Id == post.Id, post);
            }

            if (response.Type == ResponseType.User){
                user = _users.Find<User>(u => u.Id.Equals(response.Subject)).FirstOrDefault();
                user.State = UserState.Active;
                _users.ReplaceOne(u => u.Id == user.Id, user);
            }
            _responses.DeleteOne(r => r.Id == id);
        }
    }
}