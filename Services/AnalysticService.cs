using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;
using TakeIt.WebApi.Entities;
using TakeIt.WebApi.Enums;
using TakeIt.WebApi.Helpers;
using TakeIt.WebApi.IServices;
using TakeIt.WebApi.Models;
using TakeIt.WebApi.Models.Analystic;
using TakeIt.WebApi.Models.Item;
using TakeIt.WebApi.Models.Query;

namespace TakeIt.WebApi.Services
{
    public class AnalysticService : IAnalysticService
    {
        private readonly IMongoCollection<Entities.Item> _items;
        private readonly IMongoCollection<Entities.Post> _posts;
        private readonly IMongoCollection<Entities.Transaction> _transactions;
        private readonly IMongoCollection<Entities.User> _users;
        private readonly IMongoCollection<Entities.Response> _responses;

        public AnalysticService(ITakeItDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _items = database.GetCollection<Entities.Item>(settings.ItemsCollectionName);
            _posts = database.GetCollection<Entities.Post>(settings.PostsCollectionName);
            _transactions = database.GetCollection<Entities.Transaction>(settings.TransactionsCollectionName);
            _users = database.GetCollection<Entities.User>(settings.UsersCollectionName);
            _responses = database.GetCollection<Entities.Response>(settings.ResponsesCollectionName);
        }

        public OverAllDataModel GetOverAllData()
        {
            var item = new Models.Analystic.Item
            (
                (int) _items.Find<Entities.Item>(i => true).CountDocuments(),
                (int) _items.Find<Entities.Item>(i => i.State == ItemState.Pending).CountDocuments(),
                (int) _items.Find<Entities.Item>(i => i.State == ItemState.Available).CountDocuments(),
                (int) _items.Find<Entities.Item>(i => i.State == ItemState.InUse).CountDocuments(),
                (int) _items.Find<Entities.Item>(i => i.State == ItemState.Given).CountDocuments()
            );
            var post = new Models.Analystic.Post
            (
                (int) _posts.Find<Entities.Post>(i => true).CountDocuments(),
                (int) _posts.Find<Entities.Post>(i => i.State == PostState.Pending).CountDocuments(),
                (int) _posts.Find<Entities.Post>(i => i.State == PostState.Available).CountDocuments(),
                (int) _posts.Find<Entities.Post>(i => i.State == PostState.PendingTransaction).CountDocuments(),
                (int) _posts.Find<Entities.Post>(i => i.State == PostState.InTransaction).CountDocuments(),
                (int) _posts.Find<Entities.Post>(i => i.State == PostState.Locked).CountDocuments(),
                (int) _posts.Find<Entities.Post>(i => i.State == PostState.Complete).CountDocuments()
            );
            var transaction = new Models.Analystic.Transaction
            (
                (int) _transactions.Find<Entities.Transaction>(i => true).CountDocuments(),
                (int) _transactions.Find<Entities.Transaction>(i => i.State == TransactionState.Pending).CountDocuments(),
                (int) _transactions.Find<Entities.Transaction>(i => i.State == TransactionState.InTransaction).CountDocuments(),
                (int) _transactions.Find<Entities.Transaction>(i => i.State == TransactionState.Locked).CountDocuments(),
                (int) _transactions.Find<Entities.Transaction>(i => i.State == TransactionState.PendingComplete).CountDocuments(),
                (int) _transactions.Find<Entities.Transaction>(i => i.State == TransactionState.Complete).CountDocuments()
            );
            var user = new Models.Analystic.User
            (
                (int) _users.Find<Entities.User>(i => true).CountDocuments(),
                (int) _users.Find<Entities.User>(i => i.State == UserState.Pending).CountDocuments(),
                (int) _users.Find<Entities.User>(i => i.State == UserState.Active).CountDocuments(),
                (int) _users.Find<Entities.User>(i => i.State == UserState.Locked).CountDocuments(),
                (int) _users.Find<Entities.User>(i => i.State == UserState.TemporaryDeleted).CountDocuments()
            );
            var response = new Models.Analystic.Response
            (
                (int) _responses.Find<Entities.Response>(i => true).CountDocuments(),
                (int) _responses.Find<Entities.Response>(i => i.State == ResponseState.Pending).CountDocuments(),
                (int) _responses.Find<Entities.Response>(i => i.State == ResponseState.NotAccepted).CountDocuments(),
                (int) _responses.Find<Entities.Response>(i => i.State == ResponseState.Resolved).CountDocuments()
            );

            var overAllDataModel = new OverAllDataModel();
            overAllDataModel.Item = item;
            overAllDataModel.Post = post;
            overAllDataModel.Transaction = transaction;
            overAllDataModel.User = user;
            overAllDataModel.Response = response;

            return overAllDataModel;
        }

        public dynamic GetChartData(AnalysticDataType type)
        {
            var now = DateTime.Now;
            var chartDataModel = new ChartDataModel();
            // Try with datetime
            List<DateTime> dayinWeeks = DateTimeHelper.GetDaysInCurrentWeekList();
            List<int> daysInMonth = DateTimeHelper.GetDaysInCurrentMonthList();

            switch (type)
            {
                case AnalysticDataType.ThisWeek:
                    var postCounts = new List<CountByDateModel>();
                    var postTuple = _posts.Aggregate()
                        .Group
                        (
                            p => p.CreatedAt,
                            g => new CountByDateModel
                            {
                                Date = g.Key,
                                Count = g.Count()
                            }
                        )
                        .SortBy(g => g.Date)
                        .ToList();

                    int nextTupleIndex = 0;

                    foreach (var day in dayinWeeks)
                    {
                        if (day == postTuple[nextTupleIndex].Date)
                        {
                            postCounts.Add(new CountByDateModel(
                                day,
                                postTuple[nextTupleIndex].Count
                            ));

                            ++nextTupleIndex;
                        }
                        
                        else
                            postCounts.Add(new CountByDateModel(
                                day,
                                0
                            ));
                    }

                    // Transaction

                    // var transactionTuple = _transactions.Aggregate()
                    //     .Group
                    //     (
                    //         p => p.CreatedAt,
                    //         g => new
                    //         {
                    //             Key = g.Key,
                    //             Count = g.Count()
                    //         }
                    //     )
                    //     .SortBy(g => g.Key)
                    //     .ToList();

                    // var transactionTupleConverted = transactionTuple.Where(i => dayinWeeks.Contains(i.Key))
                    //     .ToList();

                    chartDataModel.PostCounts = postCounts;
                    // chartDataModel.TransactionCounts = transactionTupleConverted;

                    return chartDataModel;

                case AnalysticDataType.ThisMonth:
                    var p1 = _posts.Aggregate()
                        .Match
                        (
                            p => DateTime.Compare(p.CreatedAt, new DateTime(now.Year, now.Month, now.Day)) > 0 &&
                            DateTime.Compare(p.CreatedAt, new DateTime(now.Year, now.Month + 1, now.Day)) < 0
                        )
                        .Group(new BsonDocument
                        {
                            { "_id", "$created_at" }, { "Count", new BsonDocument("$sum", 1) } 
                        })
                        .Sort(new BsonDocument { { "_id", 1 } })
                        .ToList();
                    return p1;

                case AnalysticDataType.ThisYear:
                    break;

                default:
                    throw new Exception();
            }

            return chartDataModel;
        }
    }
}