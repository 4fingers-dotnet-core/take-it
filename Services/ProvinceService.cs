using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using TakeIt.WebApi.Entities;
using TakeIt.WebApi.Enums;
using TakeIt.WebApi.Helpers;
using TakeIt.WebApi.IServices;
using TakeIt.WebApi.Models;
using TakeIt.WebApi.Models.Post;

namespace TakeIt.WebApi.Services
{
    public class ProvinceService : IProvinceService
    {
        private readonly IMongoCollection<Province> _provinces;

        public ProvinceService(ITakeItDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _provinces = database.GetCollection<Province>(settings.ProvincesCollectionName);
        }

        public List<Province> Filter(string name) {
            var query = _provinces.AsQueryable();

            if (!string.IsNullOrEmpty(name))
                query = query.Where(p => p.Name == name);

            return query.ToList();
        }

        public Province Get(string id)
        {
            return _provinces.Find<Province>(p => p.Id == id).FirstOrDefault();
        }

        public Province Create(Province province)
        {         
            _provinces.InsertOne(province);

            return province;
        }

        public void Update(string id, Province province)
        {
            _provinces.ReplaceOne(p => p.Id == id, province);
        }

        public void Remove(Province province)
        {
            _provinces.DeleteOne(p => p.Id == province.Id);
        }

        public void Remove(string id)
        {
            _provinces.DeleteOne(p => p.Id == id);
        }
            
    }
}