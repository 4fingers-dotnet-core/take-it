using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using TakeIt.WebApi.Entities;
using TakeIt.WebApi.Enums;
using TakeIt.WebApi.Helpers;
using TakeIt.WebApi.IServices;
using TakeIt.WebApi.Models;
using TakeIt.WebApi.Models.Post;

namespace TakeIt.WebApi.Services
{
    public class PostService : IPostService
    {
        private readonly IMongoCollection<Item> _items;
        private readonly IMongoCollection<Post> _posts;
        private readonly IMongoCollection<PostCategory> _postCategories;
        private readonly IMongoCollection<User> _users;
        private readonly IMongoCollection<Province> _provinces;

        public PostService(ITakeItDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _items = database.GetCollection<Item>(settings.ItemsCollectionName);
            _posts = database.GetCollection<Post>(settings.PostsCollectionName);
            _postCategories = database.GetCollection<PostCategory>(settings.PostCategoriesCollectionName);
            _users = database.GetCollection<User>(settings.UsersCollectionName);
            _provinces = database.GetCollection<Province>(settings.ProvincesCollectionName);
        }

        public dynamic Filter(string owner, int? type, string category, string province, int? state,
            ReturnType returnType)
        {
            var query = _posts.AsQueryable();

            if (!string.IsNullOrEmpty(owner)) {
                ObjectId ownerObjId = new ObjectId(owner);
                query = query.Where(post => post.Owner.Equals(ownerObjId));
            }

            if (type.HasValue)
                query = query.Where(post => post.Type == (PostType) type);

            if (!string.IsNullOrEmpty(category)) {
                ObjectId categoryObjId = new ObjectId(category);
                query = query.Where(post => post.Category.Equals(categoryObjId));
            }

            if (!string.IsNullOrEmpty(province)) {
                ObjectId provinceObjId = new ObjectId(province);
                query = query.Where(post => post.Province.Equals(provinceObjId));
            }

            if (state.HasValue)
                query = query.Where(post => post.State == (PostState) state);

            switch (returnType)
            {
                case ReturnType.Basic:
                    return query.ToList();

                case ReturnType.Compact:
                    return ReturnModelHelper.ToCompactPostModel(query.ToList(), _users,
                        _postCategories, _provinces, _items);

                case ReturnType.Full:
                    return ReturnModelHelper.ToDetailPostModel(query.ToList(), _users,
                        _postCategories, _provinces, _items);

                default:
                    throw new System.Exception();
            }
        }

        public dynamic Get(string id, ReturnType returnType)
        {
            ObjectId postObjId = new ObjectId(id);

            var query = _posts.Find<Post>(Post => Post.Id.Equals(postObjId)).FirstOrDefault();

            switch (returnType)
            {
                case ReturnType.Basic:
                    return query;

                case ReturnType.Compact:
                    return ReturnModelHelper.ToCompactPostModel(query, _users,
                        _postCategories, _provinces, _items);

                case ReturnType.Full:
                    return ReturnModelHelper.ToDetailPostModel(query, _users,
                        _postCategories, _provinces, _items);

                default:
                    throw new System.Exception();
            }
        }

        public Post Create(Post post)
        {
            foreach (var objectId in post.Items)
            {
                var item = _items.Find<Item>(item => item.Id.Equals(objectId)).FirstOrDefault();
                item.State = ItemState.InUse;
                _items.ReplaceOne(item => item.Id.Equals(objectId), item);
            }
            
            _posts.InsertOne(post);

            return post;
        }

        public void Update(string id, Post postIn) =>
            _posts.ReplaceOne(post => post.Id == id, postIn);

        public void Remove(Post postIn)
        {
            foreach (var objectId in postIn.Items)
            {
                var item = _items.Find<Item>(item => item.Id.Equals(objectId)).FirstOrDefault();
                item.State = ItemState.Available;
                _items.ReplaceOne(item => item.Id.Equals(objectId), item);
            }

            _posts.DeleteOne(post => post.Id == postIn.Id);
        }

        public void Remove(string id)
        {
            var post = _posts.Find<Post>(post => post.Id == id).FirstOrDefault();
            
            foreach (var objectId in post.Items)
            {
                var item = _items.Find<Item>(item => item.Id.Equals(objectId)).FirstOrDefault();
                item.State = ItemState.Available;
                _items.ReplaceOne(item => item.Id.Equals(objectId), item);
            }

            _posts.DeleteOne(post => post.Id == id);
        }
            
    }
}