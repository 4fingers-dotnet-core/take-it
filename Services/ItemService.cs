using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using TakeIt.WebApi.Entities;
using TakeIt.WebApi.Enums;
using TakeIt.WebApi.Helpers;
using TakeIt.WebApi.IServices;
using TakeIt.WebApi.Models;
using TakeIt.WebApi.Models.Item;

namespace TakeIt.WebApi.Services
{
    public class ItemService : IItemService
    {
        private readonly IMongoCollection<Item> _items;
        private readonly IMongoCollection<User> _users;

        public ItemService(ITakeItDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _items = database.GetCollection<Item>(settings.ItemsCollectionName);
            _users = database.GetCollection<User>(settings.UsersCollectionName);
        }

        public List<Item> GetAll() {
            return _items.Find(item => true).ToList();
        }

        public List<Item> GetAllInList(List<ObjectId> list) {
            var items = new List<Item>();

            foreach (var i in list)
            {
                items.Add(_items.Find<Item>(item => item.Id.Equals(i)).FirstOrDefault());
            }

            return items;
        }

        public dynamic Get(string id, ReturnType returnType)
        {
            var item = _items.Find<Item>(item => item.Id == id).FirstOrDefault();

            switch (returnType)
            {
                case ReturnType.Basic:
                    return item;

                case ReturnType.Compact:
                    return ReturnModelHelper.ToCompactItemModel(item, _users);

                case ReturnType.Full:
                    return ReturnModelHelper.ToDetailItemModel(item, _users);

                default:
                    throw new System.Exception();
            }
        }
            

        public List<Item> GetByUserId(string owner)
        {
            ObjectId ownerObjId = new ObjectId(owner);

            return _items.Find(item => item.Owner.Equals(ownerObjId)).ToList();
        }

        public dynamic Filter(string owner, int? state, ReturnType returnType)
        {
            var query = _items.AsQueryable();

            if (!string.IsNullOrEmpty(owner))
            {
                ObjectId ownerObjId = new ObjectId(owner);
                query = query.Where(item => item.Owner.Equals(ownerObjId));
            }

            if (state.HasValue)
                query = query.Where(item => item.State == (ItemState) state);

            switch (returnType)
            {
                case ReturnType.Basic:
                    return query.ToList();

                case ReturnType.Compact:
                    return ReturnModelHelper.ToCompactItemModel(query.ToList(), _users);

                case ReturnType.Full:
                    return ReturnModelHelper.ToDetailItemModel(query.ToList(), _users);

                default:
                    throw new System.Exception();
            }
        }
            
        public Item Create(Item item)
        {
            _items.InsertOne(item);
            return item;
        }

        public void Update(string id, Item itemIn) =>
            _items.ReplaceOne(item => item.Id == itemIn.Id, itemIn);

        public void Remove(Item itemIn) =>
            _items.DeleteOne(item => item.Id == itemIn.Id);

        public void Remove(string id) => 
            _items.DeleteOne(item => item.Id == id);
    }
}