using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Driver;
using TakeIt.WebApi.Entities;
using TakeIt.WebApi.Enums;
using TakeIt.WebApi.IServices;
using TakeIt.WebApi.Models;

namespace TakeIt.WebApi.Services
{
    public class PostCategoryService : IPostCategoryService
    {
        private readonly IMongoCollection<PostCategory> _postCategories;

        public PostCategoryService(ITakeItDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _postCategories = database.GetCollection<PostCategory>(settings.PostCategoriesCollectionName);
        }

        public List<PostCategory> Filter(string name) {
            if (!string.IsNullOrEmpty(name))
                return _postCategories.Find(pc => pc.Name == name).ToList();
                
            return _postCategories.Find(pc => true).ToList();;
        }

        public PostCategory Get(string name) =>
            _postCategories.Find(pc => pc.Name == name).FirstOrDefault();

        public PostCategory Create(PostCategory postCategoryIn)
        {
            _postCategories.InsertOne(postCategoryIn);
            return postCategoryIn;
        }

        public void Update(string id, PostCategory postCategoryIn) =>
            _postCategories.ReplaceOne(pc => pc.Id == id, postCategoryIn);

        public void Remove(string id) => 
            _postCategories.DeleteOne(pc => pc.Id == id);
    }
}