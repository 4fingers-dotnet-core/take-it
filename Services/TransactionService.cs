using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using TakeIt.WebApi.Entities;
using TakeIt.WebApi.Enums;
using TakeIt.WebApi.Helpers;
using TakeIt.WebApi.IServices;
using TakeIt.WebApi.Models;
using TakeIt.WebApi.Models.Post;

namespace TakeIt.WebApi.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly IMongoCollection<Item> _items;
        private readonly IMongoCollection<PostCategory> _postCategories;
        private readonly IMongoCollection<Post> _posts;
        private readonly IMongoCollection<User> _users;
        private readonly IMongoCollection<Transaction> _transactions;
        private readonly IMongoCollection<Province> _provinces;

        public TransactionService(ITakeItDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _items = database.GetCollection<Item>(settings.ItemsCollectionName);
            _postCategories = database.GetCollection<PostCategory>(settings.PostCategoriesCollectionName);
            _posts = database.GetCollection<Post>(settings.PostsCollectionName);
            _users = database.GetCollection<User>(settings.UsersCollectionName);
            _transactions = database.GetCollection<Transaction>(settings.TransactionsCollectionName);
            _provinces = database.GetCollection<Province>(settings.ProvincesCollectionName);
        }

        public dynamic Filter(string userId, int? state, ReturnType returnType) {
            var query = _transactions.AsQueryable();

            if (!string.IsNullOrEmpty(userId)) {
                ObjectId userObjId = new ObjectId(userId);
                query = query.Where(transaction => transaction.Giver.Equals(userObjId)
                    || transaction.Receiver.Equals(userObjId));
            }

            if (state.HasValue)
                query = query.Where(transaction => transaction.State == (TransactionState) state);

            switch (returnType)
            {
                case ReturnType.Basic:
                    return query.ToList();

                case ReturnType.Full:
                    return ReturnModelHelper.ToDetailTransactionModel(query.ToList(), _users,
                        _postCategories, _posts, _items, _provinces);

                default:
                    throw new System.Exception();
            }
        }

        public dynamic Get(string id, ReturnType returnType)
        {
            ObjectId postObjId = new ObjectId(id);

            var query = _transactions.Find<Transaction>(transaction => transaction.Id.Equals(postObjId))
                .FirstOrDefault();

            switch (returnType)
            {
                case ReturnType.Basic:
                    return query;

                case ReturnType.Full:
                    return ReturnModelHelper.ToDetailTransactionModel(query, _users,
                        _postCategories, _posts, _items, _provinces);

                default:
                    throw new System.Exception();
            }
        }

        public Transaction Create(Transaction transaction)
        {
            // Change PostState to PendingTransaction by default
            var post = _posts.Find<Post>(p => p.Id.Equals(transaction.Post)).FirstOrDefault();

            post.State = PostState.PendingTransaction;
            _posts.ReplaceOne(p => p.Id == post.Id, post);

            _transactions.InsertOne(transaction);

            return transaction;
        }

        // Update transaction state after approval from giver / receiver or done
        public void Update(string id, Transaction transaction)
        {
            _transactions.ReplaceOne(t => t.Id == id, transaction);
        }

        public Transaction Approve(string id, Transaction transaction)
        {
            var thisTransaction = _transactions.Find<Transaction>(t => t.Id == id).FirstOrDefault();

            if (thisTransaction.State == TransactionState.Pending)
            {
                var post = _posts.Find<Post>(p => p.Id.Equals(transaction.Post)).FirstOrDefault();

                post.State = PostState.InTransaction;
                _posts.ReplaceOne(p => p.Id == post.Id, post);
                _transactions.ReplaceOne(t => t.Id == id, transaction);

                return transaction;
            }
            else
                return null;
        }


        public Transaction Finish(string id, Transaction transaction)
        {
            var thisTransaction = _transactions.Find<Transaction>(t => t.Id == id).FirstOrDefault();

            if (thisTransaction.State == TransactionState.InTransaction)
            {
                var post = _posts.Find<Post>(p => p.Id.Equals(transaction.Post)).FirstOrDefault();

                foreach (var objectId in post.Items)
                {
                    var item = _items.Find<Item>(i => i.Id.Equals(objectId)).FirstOrDefault();
                    item.State = ItemState.Given;
                    _items.ReplaceOne(i => i.Id == item.Id, item);
                }

                post.State = PostState.Complete;
                _posts.ReplaceOne(p => p.Id == post.Id, post);
                _transactions.ReplaceOne(t => t.Id == id, transaction);

                return transaction;
            }
            else
                return null;
        }

        public void Remove(Transaction transaction)
        {
            // Change PostState to Available
            var post = _posts.Find<Post>(p => p.Id.Equals(transaction.Post)).FirstOrDefault();
            post.State = PostState.Available;

            _transactions.DeleteOne(t => t.Id == transaction.Id);
        }
            
        public void Remove(string id)
        {
            var transaction = _transactions.Find<Transaction>(t => t.Id == id).FirstOrDefault();

            // Change PostState to Available
            var post = _posts.Find<Post>(p => p.Id.Equals(transaction.Post)).FirstOrDefault();
            post.State = PostState.Available;

            _transactions.DeleteOne(t => t.Id == id);
        }
    }
}