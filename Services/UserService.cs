using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using TakeIt.WebApi.Entities;
using TakeIt.WebApi.Enums;
using TakeIt.WebApi.Helpers;
using TakeIt.WebApi.IServices;
using TakeIt.WebApi.Models;
using TakeIt.WebApi.Models.User;

namespace TakeIt.WebApi.Services
{
    public class UserService : IUserService
    {
        private IMongoCollection<User> _users;
        private readonly AppSettings _appSettings;

        public UserService(ITakeItDatabaseSettings settings, IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            _users = database.GetCollection<User>(settings.UsersCollectionName);
        }

        public User Authenticate(string username, string password)
        {
            var queriedUser = _users.AsQueryable<User>().SingleOrDefault(x => x.Username == username);

            // return null if user not found
            if (queriedUser == null)
                return null;

            if (!BCrypt.Net.BCrypt.Verify(password, queriedUser.Hash))
                return null;
            else {
                // authentication successful so generate jwt token
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[] 
                    {
                        new Claim(ClaimTypes.Name, queriedUser.Id.ToString())
                    }),
                    Expires = DateTime.UtcNow.AddDays(7),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };

                if (string.IsNullOrEmpty(queriedUser.Token))
                {
                    var token = tokenHandler.CreateToken(tokenDescriptor);
                    queriedUser.Token = tokenHandler.WriteToken(token);
                    _users.ReplaceOne(user => user.Id == queriedUser.Id, queriedUser);
                }

                return queriedUser;
            }
        }
        
        public Task<List<User>> Filter(int? state, ReturnType returnType)
        {
            var query = _users.AsQueryable();

            if (state.HasValue)
                query = query.Where(u => u.State == (UserState) state);

            switch (returnType)
            {
                case ReturnType.Basic:
                    return Task.FromResult(query.ToList());

                default:
                    throw new System.Exception();
            }
            
        }
        public Task<List<BasicUserModel>> FilterBasic()
        {
            var users = _users.Find(post => true).ToList();

            return Task.FromResult(ReturnModelHelper.ToBasicUserModel(users));
        }

        public Task<User> Get(string id) =>
            Task.FromResult(_users.Find<User>(user => user.Id == id).FirstOrDefault());

        public User Create(User user)
        {
            _users.InsertOne(user);
            return user;
        }

        public void Update(string id, User userIn)
        {
            _users.ReplaceOne(u => u.Id == id, userIn);
        }
    }
}