using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Driver;
using TakeIt.WebApi.Entities;
using TakeIt.WebApi.Enums;
using TakeIt.WebApi.IServices;
using TakeIt.WebApi.Models.Item;
using TakeIt.WebApi.Models.Post;
using TakeIt.WebApi.Models.Response;
using TakeIt.WebApi.Models.Transaction;
using TakeIt.WebApi.Models.User;

namespace TakeIt.WebApi.Helpers
{
    public static class ReturnModelHelper
    {
        // User Model
        public static List<BasicUserModel> ToBasicUserModel(List<User> list)
        {
            var newList = new List<BasicUserModel>();

            foreach (var i in list)
            {
                newList.Add(
                    new BasicUserModel(i.Id, i.FirstName, i.LastName, i.Username)
                );
            }

            return newList;
        }

        public static CompactItemModel ToCompactItemModel(Item item,
            IMongoCollection<User> users)
        {
            var user = users.Find<User>(u => u.Id.Equals(item.Owner)).FirstOrDefault();

            return new CompactItemModel(
                item.Id,
                $"{user.FirstName} {user.LastName}",
                item.Name,
                item.Quantity,
                item.Unit,
                item.Images,
                item.State
            );
        }

        public static List<CompactItemModel> ToCompactItemModel(List<Item> list,
            IMongoCollection<User> users)
        {
            var newList = new List<CompactItemModel>();

            foreach (var i in list)
            {
                var user = users.Find<User>(u => u.Id.Equals(i.Owner)).FirstOrDefault();

                newList.Add(new CompactItemModel(
                    i.Id,
                    $"{user.FirstName} {user.LastName}",
                    i.Name,
                    i.Quantity,
                    i.Unit,
                    i.Images,
                    i.State
                ));
            }

            return newList;
        }

        public static DetailItemModel ToDetailItemModel(Item item,
            IMongoCollection<User> users)
        {
            return new DetailItemModel(
                item.Id,
                users.Find<User>(u => u.Id.Equals(item.Owner)).FirstOrDefault(),
                item.Name,
                item.Quantity,
                item.Unit,
                item.Images,
                item.State
            );
        }
        
        public static List<DetailItemModel> ToDetailItemModel(List<Item> list,
            IMongoCollection<User> users)
        {
            var newList = new List<DetailItemModel>();

            foreach (var i in list)
            {
                newList.Add(new DetailItemModel(
                    i.Id,
                    users.Find<User>(u => u.Id.Equals(i.Owner)).FirstOrDefault(),
                    i.Name,
                    i.Quantity,
                    i.Unit,
                    i.Images,
                    i.State
                ));
            }

            return newList;
        }
        
        // Post Model
        public static List<Item> ToItemList(List<ObjectId> list, IMongoCollection<Item> items)
        {
            var newList = new List<Item>();

            foreach (var i in list)
            {
                newList.Add(
                    items.Find<Item>(item => item.Id.Equals(i.ToString())).FirstOrDefault()
                );
            }

            return newList;
        }
        
        public static CompactPostModel ToCompactPostModel(Post post,
            IMongoCollection<User> users, IMongoCollection<PostCategory> postCategories,
            IMongoCollection<Province> provinces, IMongoCollection<Item> items)
        {
            var user = users.Find<User>(u => u.Id.Equals(post.Owner)).FirstOrDefault();

            return new CompactPostModel(
                post.Id,
                $"{user.FirstName} {user.LastName}",
                postCategories.Find<PostCategory>(pc => pc.Id.Equals(post.Category))
                    .FirstOrDefault()
                    .Name,
                provinces.Find<Province>(p => p.Id.Equals(post.Province))
                    .FirstOrDefault()
                    .Name,
                post.Type,
                post.Title,
                post.Content,
                post.Location,
                ToItemList(post.Items, items),
                post.CreatedAt,
                post.State
            );
        }
        public static List<CompactPostModel> ToCompactPostModel(List<Post> list,
            IMongoCollection<User> users, IMongoCollection<PostCategory> postCategories,
            IMongoCollection<Province> provinces, IMongoCollection<Item> items)
        {
            var newList = new List<CompactPostModel>();

            foreach (var i in list)
            {
                var user = users.Find<User>(u => u.Id.Equals(i.Owner)).FirstOrDefault();

                newList.Add(
                    new CompactPostModel(
                        i.Id,
                        $"{user.FirstName} {user.LastName}",
                        postCategories.Find<PostCategory>(pc => pc.Id.Equals(i.Category))
                            .FirstOrDefault()
                            .Name,
                        provinces.Find<Province>(p => p.Id.Equals(i.Province))
                            .FirstOrDefault()
                            .Name,
                        i.Type,
                        i.Title,
                        i.Content,
                        i.Location,
                        ToItemList(i.Items, items),
                        i.CreatedAt,
                        i.State
                    )
                );
            }

            return newList;
        }
        
        public static DetailPostModel ToDetailPostModel(Post post,
            IMongoCollection<User> users, IMongoCollection<PostCategory> postCategories,
            IMongoCollection<Province> provinces, IMongoCollection<Item> items)
        {
            return new DetailPostModel(
                post.Id,
                users.Find<User>(u => u.Id.Equals(post.Owner)).FirstOrDefault(),
                postCategories.Find<PostCategory>(pc => pc.Id.Equals(post.Category))
                    .FirstOrDefault(),
                provinces.Find<Province>(p => p.Id.Equals(post.Province))
                    .FirstOrDefault(),
                post.Type,
                post.Title,
                post.Content,
                post.Location,
                ToItemList(post.Items, items),
                post.CreatedAt,
                post.State
            );
        }
        public static List<DetailPostModel> ToDetailPostModel(List<Post> list,
            IMongoCollection<User> users, IMongoCollection<PostCategory> postCategories,
            IMongoCollection<Province> provinces, IMongoCollection<Item> items)
        {
            var newList = new List<DetailPostModel>();

            foreach (var i in list)
            {
                newList.Add(
                    new DetailPostModel(
                        i.Id,
                        users.Find<User>(u => u.Id.Equals(i.Owner)).FirstOrDefault(),
                        postCategories.Find<PostCategory>(pc => pc.Id.Equals(i.Category))
                            .FirstOrDefault(),
                        provinces.Find<Province>(p => p.Id.Equals(i.Province))
                            .FirstOrDefault(),
                        i.Type,
                        i.Title,
                        i.Content,
                        i.Location,
                        ToItemList(i.Items, items),
                        i.CreatedAt,
                        i.State
                    )
                );
            }

            return newList;
        }

        // Transaction Model
        public static DetailTransactionModel ToDetailTransactionModel(Transaction transaction,
            IMongoCollection<User> users, IMongoCollection<PostCategory> postCategories,
            IMongoCollection<Post> posts, IMongoCollection<Item> items,
            IMongoCollection<Province> provinces)
        {
            return new DetailTransactionModel(
                transaction.Id,
                users.Find<User>(u => u.Id.Equals(transaction.Giver)).FirstOrDefault(),
                users.Find<User>(u => u.Id.Equals(transaction.Receiver)).FirstOrDefault(),
                ToDetailPostModel(
                    posts.Find<Post>(p => p.Id.Equals(transaction.Post))
                        .FirstOrDefault(),
                    users,
                    postCategories,
                    provinces,
                    items
                ),
                transaction.CreatedAt,
                transaction.State
            );
        }

        public static List<DetailTransactionModel> ToDetailTransactionModel(List<Transaction> list,
            IMongoCollection<User> users, IMongoCollection<PostCategory> postCategories,
            IMongoCollection<Post> posts, IMongoCollection<Item> items,
            IMongoCollection<Province> provinces)
        {
            var newList = new List<DetailTransactionModel>();

            foreach (var i in list)
            {
                newList.Add(
                    new DetailTransactionModel(
                    i.Id,
                    users.Find<User>(u => u.Id.Equals(i.Giver)).FirstOrDefault(),
                    users.Find<User>(u => u.Id.Equals(i.Receiver)).FirstOrDefault(),
                    ToDetailPostModel(
                        posts.Find<Post>(p => p.Id.Equals(i.Post))
                            .FirstOrDefault(),
                        users,
                        postCategories,
                        provinces,
                        items
                    ),
                    i.CreatedAt,
                    i.State
                )
                );
            }

            return newList;
        }

        // Response Model
        public static DetailResponseModel ToDetailResponseModel(Response response,
            IMongoCollection<User> users, IMongoCollection<PostCategory> postCategories,
            IMongoCollection<Post> posts, IMongoCollection<Item> items,
            IMongoCollection<Province> provinces)
        {
            if (response.Type == ResponseType.Post)
            {
                return new DetailResponseModel(
                    response.Id,
                    users.Find<User>(u => u.Id.Equals(response.Reporter)).FirstOrDefault(),
                    ToDetailPostModel(
                        posts.Find<Post>(p => p.Id.Equals(response.Subject)).FirstOrDefault(),
                        users,
                        postCategories,
                        provinces,
                        items),
                    response.Content,
                    response.AdminMessage,
                    response.CreatedAt,
                    response.Type,
                    response.State
                );
            }

            return new DetailResponseModel(
                response.Id,
                users.Find<User>(u => u.Id.Equals(response.Reporter)).FirstOrDefault(),
                users.Find<User>(u => u.Id.Equals(response.Subject)).FirstOrDefault(),
                response.Content,
                response.AdminMessage,
                response.CreatedAt,
                response.Type,
                response.State
            );
        }

        public static List<DetailResponseModel> ToDetailResponseModel(List<Response> list,
            IMongoCollection<User> users, IMongoCollection<PostCategory> postCategories,
            IMongoCollection<Post> posts, IMongoCollection<Item> items,
            IMongoCollection<Province> provinces)
        {
            var newList = new List<DetailResponseModel>();

            foreach (var i in list)
            {
                if (i.Type == ResponseType.Post)
                {
                    newList.Add(
                        new DetailResponseModel(
                            i.Id,
                            users.Find<User>(u => u.Id.Equals(i.Reporter)).FirstOrDefault(),
                            ToDetailPostModel(
                                posts.Find<Post>(p => p.Id.Equals(i.Subject)).FirstOrDefault(),
                                users,
                                postCategories,
                                provinces,
                                items),
                            i.Content,
                            i.AdminMessage,
                            i.CreatedAt,
                            i.Type,
                            i.State
                        )
                    );
                }

                else
                {
                    newList.Add(
                        new DetailResponseModel(
                            i.Id,
                            users.Find<User>(u => u.Id.Equals(i.Reporter)).FirstOrDefault(),
                            users.Find<User>(u => u.Id.Equals(i.Subject)).FirstOrDefault(),
                            i.Content,
                            i.AdminMessage,
                            i.CreatedAt,
                            i.Type,
                            i.State
                        )
                    );
                }
            }

            return newList;
        }
    }
}