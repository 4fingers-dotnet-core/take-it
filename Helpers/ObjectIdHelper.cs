using System.Collections.Generic;
using MongoDB.Bson;

namespace TakeIt.WebApi.Helpers
{
    public static class ObjectIdHelper
    {
        public static List<ObjectId> FromList(List<string> list)
        {
            var newList = new List<ObjectId>();
            
            foreach (var i in list)
            {
                newList.Add(new ObjectId(i));
            }

            return newList;
        }
    }
}