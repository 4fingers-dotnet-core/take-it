using System;
using System.Collections.Generic;
using System.Globalization;
using MongoDB.Bson;
using TakeIt.WebApi.Entities;
using TakeIt.WebApi.IServices;
using TakeIt.WebApi.Models.Post;

namespace TakeIt.WebApi.Helpers
{
    public class DateTimeHelper
    {
        public static List<DateTime> GetDaysInCurrentWeekList()
        {
            CultureInfo cultureInfo = new CultureInfo("vi-VN");
            DayOfWeek currentCultureFDOW = cultureInfo.DateTimeFormat.FirstDayOfWeek;
            DateTime firstDayInWeek = DateTime.Now.Date;

            while (firstDayInWeek.DayOfWeek != currentCultureFDOW)
                firstDayInWeek = firstDayInWeek.AddDays(-1);

            DateTime lastDayInWeek = firstDayInWeek.AddDays(6);

            List<DateTime> daysinWeeks = new List<DateTime>();

            for (DateTime date = firstDayInWeek; date <= lastDayInWeek; date = date.AddDays(1))
                daysinWeeks.Add(date);
            

            // for (int i = 0; i < 7; i++)
            // {
            //     daysinWeeks.Add(firstDayInWeek.Day + i);
            // }

            return daysinWeeks;
        }

        public static List<int> GetDaysInCurrentMonthList()
        {
            DateTime now = DateTime.Now;
            List<DateTime> list = new List<DateTime>();
            DateTime date = new DateTime(now.Year, now.Month, 1);

            do
            {
                list.Add(date);
                date = date.AddDays(1);
            }
            while (date.Month == now.Month);

            List<int> result = list.ConvertAll(i => i.Day);

            return result;
        }

    }
}