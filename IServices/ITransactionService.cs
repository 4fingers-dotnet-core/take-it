using System.Collections.Generic;
using TakeIt.WebApi.Entities;
using TakeIt.WebApi.Enums;
using TakeIt.WebApi.Models.Post;

namespace TakeIt.WebApi.IServices
{
    public interface ITransactionService
    {
        dynamic Filter(string userId, int? state, ReturnType returnType);
        dynamic Get(string id, ReturnType returnType);
        Transaction Create(Transaction transaction);
        void Update(string id, Transaction transactionIn);
        Transaction Approve(string id, Transaction transactionIn);
        Transaction Finish(string id, Transaction transactionIn);
        void Remove(Transaction transactionIn);
        void Remove(string id);
    }
}