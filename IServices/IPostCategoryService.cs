using System.Collections.Generic;
using TakeIt.WebApi.Entities;

namespace TakeIt.WebApi.IServices
{
    public interface IPostCategoryService
    {
        List<PostCategory> Filter(string name);
        PostCategory Get(string id);
        PostCategory Create(PostCategory postCategory);
        void Update(string id, PostCategory postCategoryIn);
        void Remove(string id);
    }
}