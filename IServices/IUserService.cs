using System.Collections.Generic;
using System.Threading.Tasks;
using TakeIt.WebApi.Entities;
using TakeIt.WebApi.Enums;
using TakeIt.WebApi.Models.User;

namespace TakeIt.WebApi.IServices
{
    public interface IUserService
    {
        User Authenticate(string username, string hash);

        Task<List<User>> Filter(int? state, ReturnType returnType);

        Task<List<BasicUserModel>> FilterBasic();

        Task<User> Get(string id);

        User Create(User user);

        void Update(string id, User userIn);
    }
}