using System.Collections.Generic;
using System.Threading.Tasks;
using TakeIt.WebApi.Entities;
using TakeIt.WebApi.Models.User;

namespace TakeIt.WebApi.IServices
{
    public interface IProvinceService
    {
        List<Province> Filter(string name);
        Province Get(string id);
        Province Create(Province provinceIn);
        void Update(string id, Province provinceIn);
        void Remove(Province provinceIn);
        void Remove(string id);
    }
}