using System.Collections.Generic;
using MongoDB.Bson;
using TakeIt.WebApi.Entities;
using TakeIt.WebApi.Enums;
using TakeIt.WebApi.Models.Item;

namespace TakeIt.WebApi.IServices
{
    public interface IItemService
    {
        List<Item> GetAll();
        List<Item> GetAllInList(List<ObjectId> list);
        dynamic Get(string id, ReturnType returnType);
        List<Item> GetByUserId(string userId);
        dynamic Filter(string userId, int? state, ReturnType returnType);
        Item Create(Item item);
        void Update(string id, Item itemIn);
        void Remove(Item item);
        void Remove(string id);
    }
}