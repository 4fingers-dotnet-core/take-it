using System.Collections.Generic;
using TakeIt.WebApi.Entities;
using TakeIt.WebApi.Enums;
using TakeIt.WebApi.Models.Post;

namespace TakeIt.WebApi.IServices
{
    public interface IPostService
    {
        dynamic Filter(string userId, int? type, string category, string province, int? state,
            ReturnType returnType);
        dynamic Get(string id, ReturnType returnType);
        Post Create(Post Post);
        void Update(string id, Post postIn);
        void Remove(Post postIn);
        void Remove(string id);
    }
}