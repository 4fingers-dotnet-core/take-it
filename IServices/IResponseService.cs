using System.Collections.Generic;
using System.Threading.Tasks;
using TakeIt.WebApi.Entities;
using TakeIt.WebApi.Enums;
using TakeIt.WebApi.Models.User;

namespace TakeIt.WebApi.IServices
{
    public interface IResponseService
    {
        dynamic Filter(int? responseType, ReturnType returnType);
        dynamic Get(string id, ReturnType returnType);
        Response Create(Response responseIn);
        void Update(string id, Response responseIn);
        void Remove(Response responseIn);
        void Remove(string id);
    }
}