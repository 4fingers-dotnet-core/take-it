using System.Collections.Generic;
using MongoDB.Bson;
using TakeIt.WebApi.Entities;
using TakeIt.WebApi.Enums;
using TakeIt.WebApi.Models.Analystic;
using TakeIt.WebApi.Models.Item;

namespace TakeIt.WebApi.IServices
{
    public interface IAnalysticService
    {
        OverAllDataModel GetOverAllData();
        dynamic GetChartData(AnalysticDataType type);
    }
}