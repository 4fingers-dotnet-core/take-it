using System.ComponentModel.DataAnnotations;

namespace TakeIt.WebApi.Enums
{
    public enum ItemState
    {
        [Display(Name="Đang chờ xét duyệt")]
        Pending,
        [Display(Name="Sẵn sàng giao dịch")]
        Available,
        [Display(Name="Đang sử dụng")]
        InUse,
        [Display(Name="Đã được trao tặng")]
        Given
    }
}
