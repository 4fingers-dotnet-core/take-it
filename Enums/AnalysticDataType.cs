using System.ComponentModel.DataAnnotations;

namespace TakeIt.WebApi.Enums
{
    public enum AnalysticDataType
    {
        [Display(Name="Tuần này")]
        ThisWeek,
        [Display(Name="Tháng này")]
        ThisMonth,
        [Display(Name="Năm này")]
        ThisYear
    }
}
