using System.ComponentModel.DataAnnotations;

namespace TakeIt.WebApi.Enums
{
    public enum PostType
    {
        [Display(Name="Cần cho đi")]
        NeedToGiveAway,
        [Display(Name="Cần lấy")]
        NeedToTake,
    }
}
