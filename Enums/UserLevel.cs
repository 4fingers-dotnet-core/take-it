using System.ComponentModel.DataAnnotations;

namespace TakeIt.WebApi.Enums
{
    public enum UserLevel
    {
        [Display(Name="Thành viên")]
        Member,
        [Display(Name="Quản trị viên")]
        Administrator,
    }
}
