using System.ComponentModel.DataAnnotations;

namespace TakeIt.WebApi.Enums
{
    public enum ResponseType
    {
        [Display(Name="Tin")]
        Post,
        [Display(Name="Người dùng")]
        User
    }
}
