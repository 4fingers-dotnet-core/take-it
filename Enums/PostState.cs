using System.ComponentModel.DataAnnotations;

namespace TakeIt.WebApi.Enums
{
    public enum PostState
    {
        [Display(Name="Đang chờ xét duyệt")]
        Pending,
        [Display(Name="Sẵn sàng giao dịch")]
        Available,
        [Display(Name="Đang chờ giao dịch")]
        PendingTransaction,
        [Display(Name="Đang giao dịch")]
        InTransaction,
        [Display(Name="Bị khoá")]
        Locked,
        [Display(Name="Hoàn thành")]
        Complete,
    }
}
