using System.ComponentModel.DataAnnotations;

namespace TakeIt.WebApi.Enums
{
    public enum PostCategoryState
    {
        [Display(Name="Bị ẩn")]
        Hidden,
        [Display(Name="Sẵn có")]
        Available,
    }
}
