using System.ComponentModel.DataAnnotations;

namespace TakeIt.WebApi.Enums
{
    public enum ResponseState
    {
        [Display(Name="Đang chờ")]
        Pending,
        [Display(Name="Không chấp nhận")]
        NotAccepted,
        [Display(Name="Đã giải quyết")]
        Resolved
    }
}
