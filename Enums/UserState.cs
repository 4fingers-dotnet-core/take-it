using System.ComponentModel.DataAnnotations;

namespace TakeIt.WebApi.Enums
{
    public enum UserState
    {
        [Display(Name="Đang chờ xét duyệt")]
        Pending,
        [Display(Name="Đang hoạt động")]
        Active,
        [Display(Name="Bị khoá")]
        Locked,
        [Display(Name="Tạm xoá")]
        TemporaryDeleted
    }
}
