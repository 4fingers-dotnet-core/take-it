using System.ComponentModel.DataAnnotations;

namespace TakeIt.WebApi.Enums
{
    public enum ReturnType
    {
        [Display(Name="Cơ bản")]
        Basic,
        [Display(Name="Rút gọn")]
        Compact,
        [Display(Name="Đầy đủ")]
        Full
    }
}
