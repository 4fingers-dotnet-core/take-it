using System.ComponentModel.DataAnnotations;

namespace TakeIt.WebApi.Enums
{
    public enum TransactionState
    {
        [Display(Name="Đang chờ")]
        Pending,
        [Display(Name="Trong giao dịch")]
        InTransaction,
        [Display(Name="Bị khoá")]
        Locked,
        [Display(Name="Chờ xác nhận")]
        PendingComplete,
        [Display(Name="Hoàn thành")]
        Complete,
    }
}
