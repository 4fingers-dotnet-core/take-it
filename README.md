# TAKEIT API

TAKEIT API, includes user authentication and CRUD operations.

**Warning**: This is not finished yet. I'll update later.

## Technical using
- .NET Core 3.1
- JWT Bearer Authentication library of .NET Core
- MongoDB with MongoDB C# driver
- MinIO server
- BCrypt.Net-Core to create hash

## Project setup
### Installations
#### MongoDB
- Step 1: Download MongoDB Communtity edition from their [homepage](https://www.mongodb.com/), then get the connection string.
- Step 2: Create a new MongoDB process (for macOS only, in Windows or Linux distros you have to start the binary manually or create a OS service): `mongod --config /usr/local/etc/mongod.conf --fork`

#### MinIO
- Step 1: Download Minio Server and MinIO client (mc) from [here](https://min.io/).
- Step 2: Try to start MinIO server and browse the storage in web browser: `minio server <Path to data folder>`

You will see the addresses in terminal process.
- Step 3: Use `mc` to change the current using bucket policy to **public**. This is a important step. Please read [here](https://docs.min.io/docs/minio-client-complete-guide.html#policy).
### Configurations
`appsettings.json`

```
{
  "TakeItDatabaseSettings": {
    // MongoDB collection names
    "ProvincesCollectionName": "Provinces",
    ...
    // MongoDB connection info
    "ConnectionString": "mongodb://localhost:27017",
    "DatabaseName": "TakeItDb",
    // Secret string for JWT token
    "Secret": "thissecretstringshouldbereplaced"
  },
  "MinIOSettings": {
    // MinIO connection info
    "EndPoint": "127.0.0.1:9000",
    "AccessKey": "minioadmin",
    "SecretKey": "minioadmin",
    "BucketName": "takeit",
    "Location": "us-east-1"
  },
  ...
}

```
### Get dependencies
```
dotnet restore
```
### Run project:
```
dotnet run
```

## References
This project based on [this tutorial](https://jasonwatmore.com/post/2019/10/11/aspnet-core-3-jwt-authentication-tutorial-with-example-api).

## Related projects
[TAKEIT Client](https://gitlab.com/4fingers-vue/takeit-client)