using System;

namespace TakeIt.WebApi.Models.Query
{
    public class GroupChartModel
    {
        public GroupChartModel() {}
        public GroupChartModel(int key, int count)
        {
            Key = key;
            Count = count;
        }

        public int Key { get; set; }
        public int Count { get; set; }
    }
}