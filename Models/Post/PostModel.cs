using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using TakeIt.WebApi.Enums;

namespace TakeIt.WebApi.Models.Post
{
    public class PostModel
    {
        public string Id { get; set; }
        
        [Required]
        public string Owner { get; set; }

        [Required]
        public string Category { get; set; }

        [Required]
        public string Province { get; set; }

        [Required]
        public int Type { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Content { get; set; }

        [Required]
        public string Location { get; set; }

        [Required]
        public List<string> Items { get; set; }

        public DateTime CreatedAt { get; set; }

        public PostState State { get; set; }
    }
}