using System;
using System.Collections.Generic;
using MongoDB.Bson;
using TakeIt.WebApi.Enums;

namespace TakeIt.WebApi.Models.Post
{
    public class DetailPostModel
    {
        public DetailPostModel(string id, Entities.User owner, Entities.PostCategory category,
            Entities.Province province, PostType type, string title, string content, string location,
            List<Entities.Item> items, DateTime createdAt, PostState state)
        {
            Id = id;
            Owner = owner;
            Category = category;
            Province = province;
            Type = type;
            Title = title;
            Content = content;
            Location = location;
            Items = items;
            CreatedAt = createdAt;
            State = state;
        }

        public string Id { get; set; }
        public Entities.User Owner { get; set; }
        public Entities.PostCategory Category { get; set; }
        public Entities.Province Province { get; set; }
        public PostType Type { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Location { get; set; }
        public List<Entities.Item> Items { get; set; }
        public DateTime CreatedAt { get; set; }
        public PostState State { get; set; }
    }
}