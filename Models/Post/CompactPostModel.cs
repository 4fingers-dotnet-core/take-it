using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using TakeIt.WebApi.Enums;

namespace TakeIt.WebApi.Models.Post
{
    public class CompactPostModel
    {
        public CompactPostModel(string id, string owner, string category, string province, PostType type, string title, string content, string location, List<Entities.Item> items, DateTime createdAt, PostState state)
        {
            Id = id;
            Owner = owner;
            Category = category;
            Province = province;
            Type = type;
            Title = title;
            Content = content;
            Location = location;
            Items = items;
            CreatedAt = createdAt;
            State = state;
        }

        public string Id { get; set; }
        public string Owner { get; set; }
        public string Category { get; set; }
        public string Province { get; set; }
        public PostType Type { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Location { get; set; }
        public List<TakeIt.WebApi.Entities.Item> Items { get; set; }
        public DateTime CreatedAt { get; set; }
        public PostState State { get; set; }
    }
}