namespace TakeIt.WebApi.Models
{
    public class TakeItDatabaseSettings : ITakeItDatabaseSettings
    {
        public string ProvincesCollectionName { get; set; }
        public string UsersCollectionName { get; set; }
        public string ItemsCollectionName { get; set; }
        public string PostCategoriesCollectionName { get; set; }
        public string PostsCollectionName { get; set; }
        public string TransactionsCollectionName { get; set; }
        public string ResponsesCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    public interface ITakeItDatabaseSettings
    {
        string ProvincesCollectionName { get; set; }
        string UsersCollectionName { get; set; }
        string ItemsCollectionName { get; set; }
        string PostCategoriesCollectionName { get; set; }
        string PostsCollectionName { get; set; }
        string TransactionsCollectionName { get; set; }
        string ResponsesCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}