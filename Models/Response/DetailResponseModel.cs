using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using TakeIt.WebApi.Enums;

namespace TakeIt.WebApi.Models.Response
{
    public class DetailResponseModel
    {
        public DetailResponseModel(string id, Entities.User reporter, dynamic subject, string content,
            string adminMessage, DateTime createdAt, ResponseType type, ResponseState state)
        {
            Id = id;
            Reporter = reporter;
            Subject = subject;
            Content = content;
            AdminMessage = adminMessage;
            CreatedAt = createdAt;
            Type = type;
            State = state;
        }

        public string Id { get; set; }
        public Entities.User Reporter { get; set; }
        public dynamic Subject { get; set; }
        public string Content { get; set; }
        public string AdminMessage { get; set; }
        public DateTime CreatedAt { get; set; }
        public ResponseType Type { get; set; }
        public ResponseState State { get; set; }
    }
}