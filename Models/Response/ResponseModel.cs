using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using TakeIt.WebApi.Enums;

namespace TakeIt.WebApi.Models.Response
{
    public class ResponseModel
    {
        public string Id { get; set; }
        public string Reporter { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public string AdminMessage { get; set; }
        public DateTime CreatedAt { get; set; }
        public ResponseType Type { get; set; }
        public ResponseState State { get; set; }
    }
}