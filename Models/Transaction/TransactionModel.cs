using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using TakeIt.WebApi.Enums;

namespace TakeIt.WebApi.Models.Transaction
{
    public class TransactionModel
    {
        public string Id { get; set; }
        public string Giver { get; set; }
        public string Receiver { get; set; }
        public string Post { get; set; }
        public DateTime CreatedAt { get; set; }
        public TransactionState State { get; set; }
    }
}