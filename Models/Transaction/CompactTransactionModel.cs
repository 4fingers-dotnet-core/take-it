using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using TakeIt.WebApi.Enums;

namespace TakeIt.WebApi.Models.Transaction
{
    public class CompactTransactionModel
    {
        public CompactTransactionModel(string id, string giver, string receiver, Entities.Post post,
            DateTime createdAt, TransactionState state)
        {
            Id = id;
            Giver = giver;
            Receiver = receiver;
            Post = post;
            CreatedAt = createdAt;
            State = state;
        }
        
        // Contains name of Giver and Receiver, full Post entity
        public string Id { get; set; }
        public string Giver { get; set; }
        public string Receiver { get; set; }
        public Entities.Post Post { get; set; }
        public DateTime CreatedAt { get; set; }
        public TransactionState State { get; set; }
    }
}