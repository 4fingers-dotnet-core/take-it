using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using TakeIt.WebApi.Enums;
using TakeIt.WebApi.Models.Post;

namespace TakeIt.WebApi.Models.Transaction
{
    public class DetailTransactionModel
    {
        public DetailTransactionModel(string id, Entities.User giver, Entities.User receiver,
            DetailPostModel post, DateTime createdAt, TransactionState state)
        {
            Id = id;
            Giver = giver;
            Receiver = receiver;
            Post = post;
            CreatedAt = createdAt;
            State = state;
        }

        public string Id { get; set; }
        public Entities.User Giver { get; set; }
        public Entities.User Receiver { get; set; }
        public DetailPostModel Post { get; set; }
        public DateTime CreatedAt { get; set; }
        public TransactionState State { get; set; }
    }
}