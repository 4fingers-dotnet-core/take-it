using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TakeIt.WebApi.Enums;

namespace TakeIt.WebApi.Models.Analystic
{
    public class CountByDateModel
    {
        public CountByDateModel() {}

        public CountByDateModel(DateTime date, int count)
        {
            Date = date;
            Count = count;
        }

        public DateTime Date { get; set; }
        public int Count { get; set; }
    }
}