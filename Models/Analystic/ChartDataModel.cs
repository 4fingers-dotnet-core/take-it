using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TakeIt.WebApi.Enums;

namespace TakeIt.WebApi.Models.Analystic
{
    public class ChartDataModel
    {
        public ChartDataModel() {}

        public List<CountByDateModel> ItemCounts { get; set; }
        public List<CountByDateModel> PostCounts { get; set; }
        public List<CountByDateModel> TransactionCounts { get; set; }
    }
}