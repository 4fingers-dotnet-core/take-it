using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TakeIt.WebApi.Enums;

namespace TakeIt.WebApi.Models.Analystic
{
    public class Item
    {
        public Item(int all, int pending, int available, int inUse, int given)
        {
            All = all;
            Pending = pending;
            Available = available;
            InUse = inUse;
            Given = given;
        }

        public int All { get; set; }
        public int Pending { get; set; }
        public int Available { get; set; }
        public int InUse { get; set; }
        public int Given { get; set; }
    }

    public class Post
    {
        public Post(int all, int pending, int available, int pendingTransaction, int inTransaction,
            int locked, int complete)
        {
            All = all;
            Pending = pending;
            Available = available;
            PendingTransaction = pendingTransaction;
            InTransaction = inTransaction;
            Locked = locked;
            Complete = complete;
        }

        public int All { get; set; }
        public int Pending { get; set; }
        public int Available { get; set; }
        public int PendingTransaction { get; set; }
        public int InTransaction { get; set; }
        public int Locked { get; set; }
        public int Complete { get; set; }
    }

    public class Transaction
    {
        public Transaction(int all, int pending, int inTransaction, int locked, int pendingComplete,
            int complete)
        {
            All = all;
            Pending = pending;
            InTransaction = inTransaction;
            Locked = locked;
            PendingComplete = pendingComplete;
            Complete = complete;
        }

        public int All { get; set; }
        public int Pending { get; set; }
        public int InTransaction { get; set; }
        public int Locked { get; set; }
        public int PendingComplete { get; set; }
        public int Complete { get; set; }
    }

    public class User
    {
        public User(int all, int pending, int active, int locked, int temporaryDeleted)
        {
            All = all;
            Pending = pending;
            Active = active;
            Locked = locked;
            TemporaryDeleted = temporaryDeleted;
        }

        public int All { get; set; }
        public int Pending { get; set; }
        public int Active { get; set; }
        public int Locked { get; set; }
        public int TemporaryDeleted { get; set; }
    }

    public class Response
    {
        public Response(int all, int pending, int notAccepted, int resolved)
        {
            All = all;
            Pending = pending;
            NotAccepted = notAccepted;
            Resolved = resolved;
        }

        public int All { get; set; }
        public int Pending { get; set; }
        public int NotAccepted { get; set; }
        public int Resolved { get; set; }
    }


    public class OverAllDataModel
    {
        public OverAllDataModel() {}

        public OverAllDataModel(Item item, Post post, Transaction transaction, User user,
            Response response)
        {
            Item = item;
            Post = post;
            Transaction = transaction;
            User = user;
            Response = response;
        }

        public Item Item { get; set; }
        public Post Post { get; set; }
        public Transaction Transaction { get; set; }
        public User User { get; set; }
        public Response Response { get; set; }
    }
}