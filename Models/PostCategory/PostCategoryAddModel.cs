using System.ComponentModel.DataAnnotations;

namespace TakeIt.WebApi.Models.Post
{
    public class PostCategoryAddModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }
    }
}