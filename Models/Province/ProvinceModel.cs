using System.ComponentModel.DataAnnotations;

namespace TakeIt.WebApi.Models.Province
{
    public class ProvinceModel
    {
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }
    }
}