using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TakeIt.WebApi.Enums;

namespace TakeIt.WebApi.Models.Item
{
    public class ItemModel
    {
        public string Id { get; set; }
        
        public string Owner { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public float Quantity { get; set; }

        [Required]
        public string Unit { get; set; }

        public List<string> Images { get; set; }

        public ItemState State { get; set; }
    }
}