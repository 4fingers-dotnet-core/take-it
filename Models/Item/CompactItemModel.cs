using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TakeIt.WebApi.Enums;

namespace TakeIt.WebApi.Models.Item
{
    public class CompactItemModel
    {
        public CompactItemModel(string id, string owner, string name, float quantity, string unit, List<string> images, ItemState state)
        {
            Id = id;
            Owner = owner;
            Name = name;
            Quantity = quantity;
            Unit = unit;
            Images = images;
            State = state;
        }

        public string Id { get; set; }

        public string Owner { get; set; }

        public string Name { get; set; }

        public float Quantity { get; set; }

        public string Unit { get; set; }

        public List<string> Images { get; set; }

        public ItemState State { get; set; }
    }
}